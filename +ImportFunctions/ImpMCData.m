function Output =  ImpMCData(path)
% Function for importing MC data files placed at path. The files contain
% real and imaginary parts input voltage and current as well as current
% runnig through an inductor representing the velocity of that mass
%
% --------------------------- Input parameters -------------------------- %
% path                      Full path for the folder containing the data
%
% ----------------------------Output parameters ------------------------- %
% Output                    1xnFiles cell array with a struct in each cell.
%
%   Each struct contains:
%           FileName        String with the name of the measurement file
%           FileDate        String with the date of the measurement file
%           Freq            Array with measurement frequencies
%           re_XXX          Array with real part of result quantity XXX
%           im_XXX          Array with imag part of result quantity XXX
%                  XXX quantities must be reasonable named from MC

% 06.05.2020 NIFD

%% Simulation data
Files = dir([path '*.csv']);
nFiles = numel(Files);

% Import options
opts = delimitedTextImportOptions("NumVariables", 5);

% Specify range and delimiter
opts.DataLines = [1, Inf];
opts.Delimiter = ",";

%% Import data
Data = cell(1,nFiles);
for ifile = 1:nFiles
    file = [Files(ifile).folder '\' Files(ifile).name];

    tempDat = readtable(file, opts);

    % Convert to output type
    tempDat = table2cell(tempDat);

    numIdx = cellfun(@(x) ~isnan(str2double(x)), tempDat);

    tempDat(numIdx) = cellfun(@(x) {str2double(x)}, tempDat(numIdx));

    Data{ifile} = tempDat;

end
% Clear temporary variables
clear opts

%% Process data
% Preallocate output struct
Output = cell(1,nFiles);
for ifile = 1:nFiles
    Output{ifile}.FileName = Files(ifile).name;
    Output{ifile}.FileDate  = Files(ifile).date;
    
    temp = Data{ifile};
    % Number of frequency points
    iNum = strfind(temp{4,1},'=');
    nof = str2double(temp{4,1}(iNum+1:end));
    
    % Number of result quantities in file
    nores = size(temp,1)/(nof+5);
    % Matrix containing the data range for each quantity
    range = zeros(nores,nof);
    range(1,:) = 5 + [1:nof];   % First range
    freq = [temp{range(1,:),1}];    % Frequencies - same for all data
    Output{ifile}.Freq = freq;
    
    % Preallocate result name cell array
    ResNam = cell(1,nores);
    for ires = 1:nores
        if ires > 1
            range(ires,:) = range(ires-1,end) + range(1,:);
        end
        % Id of first row of a block of data
        startId = range(ires,1)-5;
        % Id of first letter in MC assigned name to this result
        namStart = strfind(temp{startId,1},'=')+1;
        % Assigned name from MC to partiular result quantity
        ResNam{ires} = temp{startId,1}(namStart:end);
        
        % save result quantity - always stored in 4th column from MC
        Output{ifile}.(ResNam{ires}) = [temp{range(ires,:),4}];
        
    end
end