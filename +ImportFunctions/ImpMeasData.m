function [Voltage,Current] = ImpMeasData(path)
% Function for importing and processing impedance measurements on 
% loudspeakers/receivers. The data must be located in path and organized as
% text files with names: "UNIQUENAME_(I/V)" for current and voltage
% measurements respectively.
%
%
% -------------------------------- INPUT -------------------------------- %
% path                      Full path for location of .txt-files
%
%
% -------------------------------- OUTPUT ------------------------------- %
% Voltage/Current           Structs containing the measured input voltage
%                           and output current with one cell array per
%                           measurement.
%
%           FileName        String with the name of the measurement file
%           FileDate        String with the date of the measurement file
%           Freq            Array with measurement frequencies
%           Comp            Complex valued measurement result
%           Mag             Measurement Magnitude
%           Phase           Measurement Phase
% ----------------------------------------------------------------------- %
%
% 12.03.2020 NIFD

% ------------------------ WORKING NOTES ---------------------------------%
% Implement an input argument for the I/V-name extension, so that it is
% optional 
%
% Change the folder structure from the one that is used now to something
% that is more structures, so that my AnalysisFile does not need a path
% variable. This is annoying
%

%% Measurement data

% File info
VoltageFiles = dir([path '*_V.txt']);
CurrentFiles = dir([path '*_I.txt']);

% Check number of files are equal
N_I = length(CurrentFiles);
N_V = length(VoltageFiles);

if N_I ~= N_V
    error(['Voltage and Current measurement files not equal.' ...
    newline ['N_Voltage = ' num2str(N_V) ', N_Current = ' num2str(N_I)]]) 
else
    N_Meas = N_I;
end

%% Import data

% FieldNames
Fnames = {'FileName','FileDate','Freq','Comp','Mag','Phase'};

% Preallocate structs
for inam = 1:numel(Fnames)
    Current.(Fnames{inam}) = cell(1,N_Meas);
end
Voltage = Current;

for imeas = 1:N_Meas
    Current.FileName{imeas} = CurrentFiles(imeas).name;
    Current.FileDate{imeas} = CurrentFiles(imeas).date;
    [Current.Freq{imeas},Current.Comp{imeas},Current.Mag{imeas}, ...
                                            Current.Phase{imeas}] = ...
        read_pulse_data([path CurrentFiles(imeas).name]);
    Current.Freq{imeas} = Current.Freq{imeas}'; 
    
    Voltage.FileName{imeas} = VoltageFiles(imeas).name;
    Voltage.FileDate{imeas} = VoltageFiles(imeas).date;
    [Voltage.Freq{imeas},Voltage.Comp{imeas},Voltage.Mag{imeas}, ...
                                            Voltage.Phase{imeas}] = ...
        read_pulse_data([path VoltageFiles(imeas).name]);
    Voltage.Freq{imeas} = Voltage.Freq{imeas}';
    
end
