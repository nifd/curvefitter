function [f, comp, mag, pha]=read_pulse_data(filename)
    fid1 = fopen(filename);
    k=0;
    tl=fgetl(fid1);
    flag_=0;
    flag_end=0;

    while ischar(tl)
        k=k+1;
        if str2num(tl(1:2))==1
            flag_=k;
        end
        if size(tl,2)>8
        if ((flag_end<1)&&(strcmp(tl(1:9),'TagsBegin')))
            flag_end=k;
            break
        end
        end
    if k==18
        k;
    end
        if ((flag_>0)&&(flag_end<1))
        out(k-(flag_-1),:)=textscan(tl,'%s %s %s %s');
        end
        tl=fgetl(fid1);
    end
    fclose(fid1);

    for k=1:size(out,1)
    temp1=char(strrep(out{k,2},',','.'));  
    F(k)=str2num(temp1);
    temp2=char(strrep(out{k,3},',','.'));
    A(k)=str2num(temp2);
    temp3=char(strrep(out{k,4},',','.'));
    B(k)=str2num(temp3);
    end

    f=F';
    mag=20*log10(abs(A+i*B));
    pha=(angle(A+1i*B))*180/pi;
    comp=A+1i*B;
end

function w=makeWeighting(f, Focus, taperLen)
    w=ones(size(f));
    [m,n]=size(Focus);
    len=length(f);
    for k = 1:n/3
        Freq    = Focus((k-1)*3+1);
        BW      = Focus((k-1)*3+2);
        Amp     = Focus((k-1)*3+3);
        
        f_low=find(f>=Freq-BW/2,1,'first');
        f_hi=find(f>=Freq+BW/2,1, 'first');        
        
        f_low_t1 = f_low - taperLen;
        if(f_low_t1 < 1)
            f_low_t1 = 1;
        end        
        f_low_t2 = f_low + taperLen;
        if(f_low_t2 > len)
            f_low_t2 = len;
        end
        
        f_hi_t1 = f_hi - taperLen;
        if(f_hi_t1 < 1)
            f_hi_t1 = 1;
        end
        
        f_hi_t2 = f_hi + taperLen;
        if(f_hi_t2 > len)
            f_hi_t2 = len;
        end 
        
        if(f_low_t2 > f_hi_t1)%taperLen is too big..
           Ovlp = f_low_t2 - f_hi_t1;
           f_low_t2 = f_low_t2 - Ovlp/2;
           f_hi_t1 = f_hi_t1 + Ovlp/2;
        end
        
        LoLevel = 1;%w(f_low_t1);
        if(f_low > taperLen)
            w(f_low_t1:f_low_t2) = Taper(LoLevel, Amp, f_low_t1, f_low_t2)';
        else
            w(f_low_t1:f_low_t2) = Amp;
        end;
        w(f_hi_t1:f_hi_t2) = w(f_hi_t1:f_hi_t2)+Taper(Amp,LoLevel, f_hi_t1, f_hi_t2)';
        w(f_low_t2+1:f_hi_t1-1)=w(f_low_t2+1:f_hi_t1-1)+Amp;
    end
end


