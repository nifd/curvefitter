function VibData = VibMeasData(path)
% Function for importing and processing measure vibration data from the
% PolyTec setup in Holland. The .dat files must be located under path. 
% The measurement files should contain 10 columns:
% [#pos, posx, posy, SOMEFLAG, Freq, VoltMag, VoltPhase, DispMag,
% DispPhase, CoherenceMag]
% If this is not what is seen in the .Header-output, something is wrong.
%
%
% -------------------------------- INPUT -------------------------------- %
% path                      Full path for location of .txt-files
%
%
% -------------------------------- OUTPUT ------------------------------- %
% VibData                   Struct containing the measured data with one
%                           cell array per measurement file, KK
%
%           FileName{KK}    Name of measurement file
%           Header{KK}      Header in the measurement file
%
%           .Freq{KK}       1xN Frequency Array
%           .dispMag{KK}    NxM Array of displacement amplitudes
%                           M is measurement points
%           .dispPha{KK}    NxM Array of displacement phases
%           .voltMag{KK}    NxM Array of input voltage amplitudes
%           .voltPha{KK}    NxM Array of input voltage phases
%           .coh{KK}        NxM Array of displacement/voltage magnitude
%                           coherence
%           .posx/posy      1xM array of x,y coordinates of meausrement
%                           point
%           .Disp{KK}       NxM Array of complex displacements
%           .Vel{KK}        NxM Array of complex velocities
%           .Acc{KK}        NxM Array of complex accelerations
%           .Volt{KK}       NxM Array of complex voltage inputs
%
% ----------------------------------------------------------------------- %
%
% 12.03.2020 NIFD

% ------------------------ WORKING NOTES ---------------------------------%

%

%% Measurement data

% File info
files = dir([path '*.dat']);
% keyboard

% Number of measurements
N_file = length(files);


%% Import and process data

for KK = 1:N_file
    % Import measurement data
    Meas = importdata([path files(KK).name],'\t',10);
    data = Meas.data;
    header = Meas.textdata;
    % Identify the shifts between measurement points in the data
    shift = diff(data(:,1));
    idshift = find(shift);
    
    % Check equal frequency distribution
    if range(diff(idshift))~= 0
        error([['Different frequency spacing for different measurement points.'] ...
            [newline 'Corrupted measurement file: ' files(KK).name]])
    end
    
    % Insert first and last element in the idshift array
    idshift = [0 idshift' size(data,1)];
    
    % Measurement filename
    VibData.FileName{KK} = files(KK).name;
    VibData.Header{KK} = header;
    
    % Frequency spacing
    VibData.Freq{KK} = data(idshift(1)+1:idshift(2),5);
    
    % Number of measurement points
    N_points = length(idshift) - 1;
    keyboard
    % Loop over measurement points
    for k = 1:N_points
        VibData.dispMag{KK}(:,k)=data(idshift(k)+1:idshift(k+1),6);
        VibData.dispPha{KK}(:,k)=data(idshift(k)+1:idshift(k+1),7);
        VibData.voltMag{KK}(:,k)=data(idshift(k)+1:idshift(k+1),8);
        VibData.voltPha{KK}(:,k)=data(idshift(k)+1:idshift(k+1),9);
        VibData.coh{KK}(:,k)=data(idshift(k)+1:idshift(k+1),10);
        VibData.posx{KK}(k)=data(idshift(k)+1,2);
        VibData.posy{KK}(k) = data(idshift(k)+1,3);
        
        % complex measured displacement
        VibData.Disp{KK}(:,k)=VibData.dispMag{KK}(:,k).*cos(VibData.dispPha{KK}(:,k)./180*pi) ...
            +1i*VibData.dispMag{KK}(:,k).*sin(VibData.dispPha{KK}(:,k)./180*pi);
        
        VibData.Vel{KK}(:,k) = 1i*2*pi*VibData.Freq{KK}.*VibData.Disp{KK}(:,k);
        VibData.Acc{KK}(:,k) = 1i*2*pi*VibData.Freq{KK}.*VibData.Vel{KK}(:,k);
        
        % complex measured voltage
        VibData.Volt{KK}(:,k)=VibData.voltMag{KK}(:,k).*cos(VibData.voltPha{KK}(:,k)./180*pi) ...
            +1i*VibData.voltMag{KK}(:,k).*sin(VibData.voltPha{KK}(:,k)./180*pi);
    end
end