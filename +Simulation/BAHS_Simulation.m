function X = BAHS_Simulation(f,P,Uinp)
% Calculate the response of the BAHS device, given the forcing F and the 
% parameters P
%
% 06.05.2020 NIFD

% Unfold parameters
Le = P(1);
Re = P(2);
T = P(3);
mcw = P(4);
mskull = P(5);
kvib = P(6);
bvib = P(7);

if length(P)>7
    ks1 = P(8);
    ms1 = P(9);
    ks2 = P(10);
    ms2 = P(11);
else
    ks1 = 0;
    ms1 = 0;
    ks2 = 0;
    ms2 = 0;
end



% Reduce problem size if suspension springs are excluded
if ks1 == 0 && ms1 == 0 && ks2 == 0 && ms2 == 0
    DOF = 3;
elseif ks1 > 0 && ms1 > 0 && ks2 > 0 && ms2 > 0
    DOF = 5;
else
    disp('WARNING: Inconsistent parameter values for suspension springs')
end
% keyboard
omega = 2*pi*f;
nof = length(omega);

F = zeros(DOF,nof);
F(1,:) = Uinp;

for ifrek = 1:nof
    
      [A] = SysMatBAHSVibrator_w_SuspSprings(Re,Le,T,kvib,ks1,ks2,bvib,mskull,mcw,ms1,ms2,omega(ifrek));
    if DOF == 3
        A = A(1:3,1:3);
    end
    X(:,ifrek) = A\F(:,ifrek);
end


if DOF == 5
    Output(3,:) = X(4,:);
    Output(4,:) = X(5,:);
end