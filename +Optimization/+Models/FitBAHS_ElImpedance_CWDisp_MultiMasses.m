function Output = FitBAHS_ElImpedance_CWDisp_MultiMasses(f,P,m_add)
% The electrical impedance and counterweight vibrations in two rows,
% Output = [Z_el(f);x_CW(f)/U]
% Input paramters, freq, f; parameters, P, added mass to CW, m_add

% Unfold parameters
Le = P(1);
Re = P(2);
T = P(3);
mcw = P(4);
mskull = P(5);
kvib = P(6);
bvib = P(7);


omega = 2*pi*f;
nof = length(omega);

% Number of known mass additions
n_masses = length(m_add);

% Preallocate response quantities
X2 = zeros(n_masses,nof);
X1 = X2;
Z_el = X2;
%% Compact parameters
% keyboard
for imass = 1:n_masses
ki = Re + 1i*omega*Le;
k1i = kvib + 1i*omega*bvib - omega.^2*mskull + 1i*omega*T^2./ki;
k2i = kvib + 1i*omega*bvib - omega.^2*(mcw+m_add(imass)) + 1i*omega*T^2./ki;
kci = kvib + 1i*omega*bvib + 1i*omega*T^2./ki;

% disp(['m_add = ' num2str(m_add(imass)) ', f_n = '  num2str(sqrt(kvib*(mcw + m_add(imass)+mskull)/((mcw+ m_add(imass))*mskull))/(2*pi))]);

%% Calculate result quantities

% Eq. (F1)
X2(imass,:) = T./ki.*((kci./k1i - 1)./(k2i - kci.^2./k1i));

% Eq. (F2)
X1(imass,:) = (T./ki + kci.*X2(imass,:))./k1i;

% Eq. (F3)
Z_el(imass,:) = ki./(1-1i*omega.*T.*(X1(imass,:) - X2(imass,:)));
% Z_el(imass,:) = (1./ki + 1i*omega.*T.*(X1(imass,:) - X2(imass,:))).^(-1);
end
%% Put in output struct
Output =  [Z_el;X2];