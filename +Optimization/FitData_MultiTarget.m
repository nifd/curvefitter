function P=FitData_MultiTarget(FunHandle, f, target, P_start, m_add, ActiveParams, Weighting)

% Developer flags
PlotParameterOptimization = 1; % Show variation of parameter with iterations

FitFun = FunHandle;
if(nargin <7)
    weighting = ones(size(f));
else
    weighting = makeWeighting(f, Weighting,1);
end

% Find peaks in target curves
for itarget = 1:size(target,1)
    [peakloc,peakMag] = Optimization.SupportFunctions.peakfinder(20*log10(abs(target(itarget,:))),1/4,[],1,1,1);
    % Remove final value
    if peakloc(end) == length(f)
        peakloc(end) = [];
        peakMag(end) = [];
    end
    if peakloc(1) == 1
        peakloc(1) = [];
        peakMag(1) = [];
    end

    % Create ErrPars struct with peak freqs and vals. For cost function
    ErrPars.PeakFreq{itarget} = f(floor(peakloc)) + ...
                                diff(f(floor(peakloc):ceil(peakloc))).* ...
                                (peakloc - floor(peakloc));
    ErrPars.PeakVal{itarget} = peakMag;
    
end

% Initialize parameter array
P=P_start;

Ntarget = 4;
% keyboard
NumActiveParms = length(ActiveParams(find(ActiveParams==1,length(ActiveParams))));

est = FitFun(f,P,m_add);
% keyboard
DD=[];
D0=Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, est, target,ErrPars);
%     keyboard
MaxIterations = 160;
N=1.25;
N_decay = 1.025;
plot_update=1;

M = 20;

ii=1;ii_max=MaxIterations;
[NumSets, NumPars] = size(P);

if(NumSets == 1)
    P=P';
end

ErrorEst = zeros(1, (M-1)*ii_max+(ii_max-1)*NumPars);
done = 0;
donecnt = 0;
Nmax = 0;
Nmin = inf;
% P_it = zeros(MaxIterations,length(P));
% ErrorEstVis = zeros(1,MaxIterations);
ErrorEstVis(1) = D0;
P_it(1,:) = P;
itCount = 1;
for mm=1:M         % Loop for scaling factor update type 1
    ii=1;
    N=1+(M-mm)*0.025; % Outer loop scaling factor update
    
    while (ii<ii_max)
        ii=ii+1;
        N=(N-1)/N_decay+1;  % Inner loop scaling factor update (inner iterations)
        ErrorEst(end-10:end);
        pInx = 0;
        
        if N > Nmax 
            Nmax = N;
        end
        if N < Nmin 
            Nmin = N;
        end
        
        for jj=1:NumPars
            for kk=1:NumSets
                if(ActiveParams(jj)== 1)
                    pInx = pInx + 1;
                    % Update parameter arrays - increase and
                    % decrease by decay factor N
                    Pa=P;Pa(jj,kk)=Pa(jj,kk)*N;
                    Pb=P;Pb(jj,kk)=Pb(jj,kk)/N;
                    % Function estimates with two parameter sets
                    esta=FitFun(f,Pa,m_add);
                    estb=FitFun(f,Pb,m_add);
                    % Errors for estimated functions
                    Da=Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, esta, target,ErrPars);
                    Db=Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, estb, target,ErrPars);
%                     keyboard
                    % Update error limits and parameter array
                    if Da<Db && Da<D0
                        P=Pa;D0=Da;est=esta;
                    elseif Db<Da && Db<D0
                        P=Pb;D0=Db;est=estb;
                    end
                    % Count iterations
                    itCount = itCount + 1;
                    % Store paramer array for each iteration for tracing
                    % the performance of the optimizer
                    P_it(itCount,:) = P;
                    % Update error estimate array
                    ErrorEstVis(itCount) = D0;
                    LastPos = (mm-1)*ii_max+(ii-1)*NumActiveParms+pInx;
                    ErrorEst(LastPos) = D0;
                end
            end
        end

        % Visualize curvefitter/optimization procedure every 25 iterations
        if mod(ii,25)==0
            if(LastPos <= 25)
                break;
            else
                minEst = min(ErrorEst(LastPos-25:LastPos));
                maxEst = max(ErrorEst(LastPos-25:LastPos));
                if(minEst == maxEst)
                    done = 1;
                    
                end
               
                
                subplot(4,1,1);
                semilogx(f,20*log10(abs(target(1,:))),'b',f,20*log10(abs(est(1,:))),'r',f,20*log10(max(abs(target(1,:)))*weighting/max(weighting)),'g')
                hold on
                semilogx(ErrPars.PeakFreq{1},ErrPars.PeakVal{1},'o')
                hold off
                title(sprintf('D_{ave} = %2.3e,  N = %g, M = %g, ii = %g',D0/length(f),N,mm,ii))
                grid on
                axis tight
                
                subplot(4,1,2)
                semilogx(f,20*log10(abs(target(2,:))),'b',f,20*log10(abs(est(2,:))),'r',f,20*log10(max(abs(target(2,:)))*weighting/max(weighting)),'g')
                hold on
                semilogx(ErrPars.PeakFreq{2},ErrPars.PeakVal{2},'o')
                hold off
                title(sprintf('D_{ave} = %2.3e,  N = %g, M = %g, ii = %g',D0/length(f),N,mm,ii))
                grid on
                axis tight
                if size(target,1)>2
                  subplot(4,1,3)
                semilogx(f,20*log10(abs(target(3,:))),'b',f,20*log10(abs(est(3,:))),'r',f,20*log10(max(abs(target(3,:)))*weighting/max(weighting)),'g')
                hold on
                semilogx(ErrPars.PeakFreq{3},ErrPars.PeakVal{3},'o')
                hold off
                title(sprintf('D_{ave} = %2.3e,  N = %g, M = %g, ii = %g',D0/length(f),N,mm,ii))
                grid on
                axis tight
                
                subplot(4,1,4)
                semilogx(f,20*log10(abs(target(4,:))),'b',f,20*log10(abs(est(4,:))),'r',f,20*log10(max(abs(target(3,:)))*weighting/max(weighting)),'g')
                hold on
                semilogx(ErrPars.PeakFreq{4},ErrPars.PeakVal{4},'o')
                hold off
                title(sprintf('D_{ave} = %2.3e,  N = %g, M = %g, ii = %g',D0/length(f),N,mm,ii))
                grid on
                axis tight
                end
%                 keyboard
%                 subplot(3,1,2)
%                 semilogx(f,20*log10(abs(target(targetno,:)))-20*log10(abs(est(targetno,:))),'r')
%                 title(sprintf('Difference'))
%                 grid on
%                 axis tight
                pause(0.01)
                if (D0/length(f))<0.001
                    ii=ii_max;
                end
%                 subplot(3,1,3)
%                 plot(ErrorEst);%(1:LastPos));
               
                
            end
        end
        
        
    end
    % Seems like the donecnt should be replaced by the mm loop?
    if(done == 1)
        donecnt = donecnt+1;
        if(donecnt > M)
            break;
        end
    end
end

if PlotParameterOptimization == 1
figure
subplot 311
plot(1:length(ErrorEstVis),ErrorEstVis)
ylabel('\phi')

subplot 312
plot(1:length(ErrorEstVis),P_it(:,4))
hold on
plot([1 length(ErrorEstVis)],[1 1]*12e-3)
plot([1 length(ErrorEstVis)],[1 1]*13e-3)
hold off
ylabel('CW mass, [kg]')

subplot 313
plot(1:length(ErrorEstVis),P_it(:,6))
hold on
plot([1 length(ErrorEstVis)],[1 1]*2.5e5)
plot([1 length(ErrorEstVis)],[1 1]*2e5)
hold off
ylabel('Stiffness, [N/m]')

figure
plot3(P_it(:,4)/13e-3,P_it(:,6)/2e5,ErrorEstVis)
xlabel('m_j/m_t')
ylabel('k_j/k_t')
zlabel('\phi')
xlim([0.5 3])
ylim([0.5 2])


figure
plot(P_it(:,4)/13e-3,P_it(:,6)/2e5,'b')
hold on
plot(P_it(1,4)/13e-3,P_it(1,6)/2e5,'ro')
plot(P_it(end,4)/13e-3,P_it(end,6)/2e5,'kx')
hold off
xlabel('m_j/m_t')
ylabel('k_j/k_t')
xlim([0.5 3])
ylim([0.5 2])

end

% % errest_end = Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting,est,target);
% errplot1 = 20*log10(abs(target(1,:)))-20*log10(abs(est(1,:)));
% errplot2 = 20*log10(abs(target(2,:)))-20*log10(abs(est(2,:)));
% normerror = 0;
% for itarget = 1:size(target,1)
%     normerror = normerror + norm(target(itarget,:) -est(itarget,:))/ ...
%                                 norm(target(itarget,:));
% end
% 
% figure
% subplot 211
% semilogx(f,errplot1)
% text(f(round(0.5*length(f))),0.75*max(errplot1),['Error = ' num2str(normerror)])
% 
% subplot 212
% semilogx(f,errplot2)
% 
% title('Error in dB')
% 
% figure
% subplot 311
% plot(ErrorEst)

%     weighting
% Nmax
% Nmin
end

% function e = Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(freq,weight,est,target,ErrPars)
% 
% % Multi-target function
% 
% e1 = 0; e2 = 0; e3 = 0; e4 = 0; e5 = 0;
% nof = length(freq);
% % keyboard
% for itarget = 1:size(target,1)
% %     keyboard
% %     norm1 = sum(20*log10(weight.*abs(target(itarget,:))));
% %     e1_current = 1/(nof*norm1)*sum(abs(20*log10(weight.*abs(est(itarget,:)))-20*log10(weight.*abs(target(itarget,:)))));
% %     e1 = e1 + e1_current;
% %     e1_current = 1/nof*sum(abs((weight.*(abs(est(itarget,:))-abs(target(itarget,:)))./(weight.*abs(target(itarget,:))))));
%     % Absolute value of difference between curves
% %     e1_current = 1/nof*sum(abs(20*log10(weight.*abs(est(itarget,:)))-20*log10(weight.*abs(target(itarget,:)))));
%     % Square the objective
%     e1_current = 1/nof*sum((20*log10(weight.*abs(est(itarget,:)))-20*log10(weight.*abs(target(itarget,:)))).^2);
%     e1 = e1 + e1_current;
% %     keyboard
% %     norm2 = max(20*log10(abs(target(itarget,:))));
% %     e2_current = 1/norm2*max(abs(20*log10(weight.*abs(est(itarget,:)))-20*log10(weight.*abs(target(itarget,:)))));
% %     
% %     e2_current = max(abs((weight.*(abs(est(itarget,:))-abs(target(itarget,:)))./(weight.*abs(target(itarget,:))))));
% %     e2_current = max(abs(20*log10(weight.*abs(est(itarget,:)))-20*log10(weight.*abs(target(itarget,:)))));
% %     
% %     e2 = e2 + e2_current;
% 
%     % Weigh in errors on peak frequency
% %     [maxFreq_t,maxVal_t]=peakExtrap(freq',target(itarget,:));
% %     [maxFreq_e,maxVal_e]=peakExtrap(freq',est(itarget,:));
%     % Use matlab function findpeaks for faster comp.
% %     [maxVal_t,maxFreqID_t,peakwidth_t] = findpeaks(20*log10(abs(target(itarget,:))),'Threshold',0.1,'Widthreference','halfprom');
% %     [maxVal_e,maxFreqID_e,peakwidth_e] = findpeaks(20*log10(abs(est(itarget,:))),'Threshold',0.1,'Widthreference','halfprom');
% %     npeaks = length(maxVal_t);
% %     keyboard
% %     maxFreq_t = freq(maxFreqID_t); maxFreq_e = freq(maxFreqID_e);
% %     maxFreq_t = ErrPars.PeakFreq{itarget}; maxVal_t = ErrPars.PeakVal{itarget};
% %     [maxVal_e,maxFreq_e] = findpeaks(20*log10(abs(est(itarget,:))),freq); % no noise on simulated signal
% %     
%     [peakloc,peakMag] = Optimization.SupportFunctions.peakfinder(20*log10(abs(est(itarget,:))),1/4,[],1,1,1);
%     % Remove final value
%     if peakloc(end) == length(freq)
%         peakloc(end) = [];
%         peakMag(end) = [];
%     end
%     if peakloc(1) == 1
%         peakloc(1) = [];
%         peakMag(1) = [];
%     end
%     maxFreq_e = freq(floor(peakloc)) + ...
%                                 diff(freq(floor(peakloc):ceil(peakloc))).* ...
%                                 (peakloc - floor(peakloc));
%     maxFreq_t = ErrPars.PeakFreq{itarget};
%     
%     maxVal_e = peakMag;
%     maxVal_t = ErrPars.PeakVal{itarget};
%    
%     
%     %     maxValLin_t = abs(target(itarget,maxFreqID_t)); maxValLin_e = abs(est(itarget,maxFreqID_e));
%     % Error on peak frequency
% %     norm3 = maxFreq_t;
% %     e3_current = 1/(npeaks)*sum(abs((maxFreq_e - maxFreq_t)./maxFreq_t));
% %     e3_current = sum(abs(maxFreq_e - maxFreq_t));
%     % Square the objective
%     e3_current = sum((maxFreq_e - maxFreq_t).^2);
%     if ~isnan(e3_current)
%         e3 = e3 + e3_current;
%     else
%         disp('WARNING: Nan values for frequency error est. Consider threshold for peaks')
%     end
%     % Error on peak value
% %     norm4 = maxValLin_t;
% %     e4_current = 1/(npeaks)*sum(abs((maxValLin_e - maxValLin_t)./maxValLin_t));
% %     e4_current = sum(abs(maxVal_e - maxVal_t));
%     % Square the difference
%     e4_current = sum(abs(maxVal_e - maxVal_t));
%      if ~isnan(e4_current)
%     e4 = e4 + e4_current;
%      else
%          disp('WARNING: Nan values for peak values est. Consider threshold for peaks')
%      end
% %      keyboard
% %      itarget
% %      e1_current
% %      e2_current
% %      e3_current
% %      e4_current
% 
% % figure
% % semilogx(freq,20*log10(abs(est(itarget,:))))
% % hold on
% % semilogx(freq,20*log10(abs(target(itarget,:))),'--r')
% % semilogx(maxFreq_e,maxVal_e,'x')
% % semilogx(maxFreq_t,maxVal_t,'ro')
% % hold off
% %      keyboard
%      % Error on peak width
%     %e5 = e5 + sum(abs(peakwidth_e-peakwidth_t));
% end
% % keyboard
% e=1*e1+1*e2 + 1*e3 + 1*e4 + 1*e5;
% 
% 
% end

function T=Taper(Level1, Level2, StartInx, EndInx)
if(Level1 <= Level2)
    t=-pi/2:pi./(EndInx-StartInx):pi/2;
    T=(Level2-Level1)*(0.5*(sin(t)+1))+Level1;
else
    t=pi/2:-pi./(EndInx-StartInx):-pi/2;
    T=(Level1-Level2)*(0.5*(sin(t)+1))+Level2;
end
end