function w=makeWeighting(f, Focus, taperLen)
% Function for creating a weighting array, w, in order to make certain
% frequencies weigh more in the curvefitter. 
%
% -------------------- Input parameters --------------------------------- %
% f                 Measured/fitted frequencies
%
% Focus             Array with weighting information: 
%                   Focus_i = [Freq_i, BandWidth_i, Amplification_i]
%                   Hence, Frequency no i is amplified in importance by
%                   Amplification_i affecting BandWidth_i.
%
% taperLen          Tapering length of the amplification. Uses a half sine
%                   wave for slowly ramping up and down the weighting.
%
% --------------------- Output parameters ------------------------------- %
% w                 Weigting array of length w with weights at desired
%                   frequencies
%
% ----------------------------------------------------------------------- %

% 17.03.2020 - COMMENTS by NIFD - Legacy code.


    w=ones(size(f));
    [m,n]=size(Focus);
    len=length(f);
    for k = 1:n/3
        Freq    = Focus((k-1)*3+1); % Amplification frequency number k
        BW      = Focus((k-1)*3+2); % Bandwidth for frequency number k
        Amp     = Focus((k-1)*3+3); % Amplification at Frequency k
        
        % Indices of upper and lower BW limits
        f_low=find(f>=Freq-BW/2,1,'first');
        f_hi=find(f>=Freq+BW/2,1, 'first');        
        
        % Start tapering before lower limit
        f_low_t1 = f_low - taperLen;
        if(f_low_t1 < 1)    % If tapering starts before first element
            f_low_t1 = 1;
        end        
        % End tapering "taperLen" inside the BW. 
        f_low_t2 = f_low + taperLen;
        if(f_low_t2 > len)
            f_low_t2 = len;
        end
        
        % Start tapering down "taperLen" before upper limit of BW
        f_hi_t1 = f_hi - taperLen;
        if(f_hi_t1 < 1)     % If tapering starts before the first frequency
            f_hi_t1 = 1;
        end
        
        f_hi_t2 = f_hi + taperLen;
        if(f_hi_t2 > len)
            f_hi_t2 = len;
        end 
        
        if(f_low_t2 > f_hi_t1)%taperLen is too big..
           Ovlp = f_low_t2 - f_hi_t1;
           f_low_t2 = f_low_t2 - Ovlp/2;
           f_hi_t1 = f_hi_t1 + Ovlp/2;
        end
        
        LoLevel = 1;%w(f_low_t1);
        if(f_low > taperLen)
            w(f_low_t1:f_low_t2) = Taper(LoLevel, Amp, f_low_t1, f_low_t2)';
        else
            w(f_low_t1:f_low_t2) = Amp;
        end;
        keyboard
        w(f_hi_t1:f_hi_t2) = w(f_hi_t1:f_hi_t2)+Taper(Amp,LoLevel, f_hi_t1, f_hi_t2)';
        w(f_low_t2+1:f_hi_t1-1)=w(f_low_t2+1:f_hi_t1-1)+Amp;
        
    end
end

function T=Taper(Level1, Level2, StartInx, EndInx)
    if(Level1 <= Level2)
        t=-pi/2:pi./(EndInx-StartInx):pi/2;
        T=(Level2-Level1)*(0.5*(sin(t)+1))+Level1;
    else
        t=pi/2:-pi./(EndInx-StartInx):-pi/2;
        T=(Level1-Level2)*(0.5*(sin(t)+1))+Level2;
    end
end
