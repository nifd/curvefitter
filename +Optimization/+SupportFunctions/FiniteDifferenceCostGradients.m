function [Grad,Hess] = FiniteDifferenceCostGradients(x,ActiveInd,x_loads,freq,weight,est,target,cost,ErrPars)
% Function for evaluating the gradients and Hessians of the cost functions
% via finite differences. 
%
% -------------------------------- INPUT -------------------------------- %
% x                             Array of independent variables for the
%                               problem
% 
% ActiveInd                     List of indices describing which
%                               independent variables are active in the
%                               optimization
%
% x_loads                       Array of added "loads" to the measured
%                               data. This represents a known parameter
%                               addition/change to the measured system,
%                               i.e. an extra mass or different end
%                               impedance. MUST BE OF A FORM COMPATIBLE
%                               WITH THE est - FUNCTION HANDLE
%
% freq                          1xnof Array of frequencies
%
% weight                        1xnof Array of weights
%
%
% est                           Function handle for the estimated
%                               curves
%
% target                        n x nof array with the target curves1000
%
% cost                          Function handle for the cost
%                               function. Must be svalar
%
% ErrPars                       Struct containing information extracted
%                               about the target curves needed in the cost
%                               function.
%
% -------------------------------- OUTPUT ------------------------------- %
% Grad                          Gradient of cost function
%
% Hess                          Hessian of cost function
%
% ----------------------------------------------------------------------- %

% ------------------------------ WORKING NOTES -------------------------- %
% It would be very nice with some generalization, so that it is not fixed
% to a vibrator with added masses. However, the estimated function require
% this input, and they need to be evaluated in this script. Perhaps a
% general "extra parameter" array, that is then interpreted by the
% particular estimating function.
% ----------------------------------------------------------------------- %

% Initialize variables
NumActiveParms = length(ActiveInd); % Number of active optimization variables
epsilon = 1e-5;  % Relative finite difference
Grad = zeros(NumActiveParms,1);
Hess = zeros(NumActiveParms);

for jpar = 1:NumActiveParms
    % Initialize backwards and forward variables
    xm = x;
    xp = x;
    
    % Finite difference step
    h1 = epsilon*x(ActiveInd(jpar));
    
    % Estimate backwards and forward variables
    xm(ActiveInd(jpar)) = xm(ActiveInd(jpar)) - h1;
    xp(ActiveInd(jpar)) = xp(ActiveInd(jpar)) + h1;
    
    % Estimate backwards and forward curves
    est_m = est(freq,xm,x_loads);
    est_p = est(freq,xp,x_loads);
    
    % Estimate backwards and forward cost function values
    Phi_m = cost(freq,weight, est_m, target,ErrPars);
    Phi_p = cost(freq,weight, est_p, target,ErrPars);
    
    % Central difference gradient
    Grad(jpar) = (Phi_p - Phi_m)/(2*h1);
    
    % Initialize total parameters for the Hessian calc
    xmtot = xm;
    xptot = xp;
    
    % Additional loop for calculating the Hessian
    for ipar = 1:NumActiveParms 
        
        % Initialize backwards and forward variables
        xmm = xmtot;
        xpp = xptot;
        
        xmp = xmtot;
        xpm = xptot;

        
        % Finite difference step
        h2 = epsilon*x(ActiveInd(ipar));
        
        % Estimate forward, backwards and cross variables
        xmm(ActiveInd(ipar)) = xmm(ActiveInd(ipar)) - h2;
        xpp(ActiveInd(ipar)) = xpp(ActiveInd(ipar)) + h2;
        xmp(ActiveInd(ipar)) = xmp(ActiveInd(ipar)) + h2;
        xpm(ActiveInd(ipar)) = xpm(ActiveInd(ipar)) - h2;
        
        
        % Estimate backwards, forward and cross curves
        est_mm = est(freq,xmm,x_loads);
        est_pp = est(freq,xpp,x_loads);
        est_mp = est(freq,xmp,x_loads);
        est_pm = est(freq,xpm,x_loads);
        
        
        % Estimate backwards, forward and cross cost function values
        Phi_mm = cost(freq,weight, est_mm, target,ErrPars);
        Phi_pp = cost(freq,weight, est_pp, target,ErrPars);
        Phi_mp = cost(freq,weight, est_mp, target,ErrPars);
        Phi_pm = cost(freq,weight, est_pm, target,ErrPars);
        
        % Hessian Calc
        Hess(jpar,ipar) = (Phi_pp + Phi_mm - Phi_mp - Phi_pm)/(4*h1*h2);
    end
end

    