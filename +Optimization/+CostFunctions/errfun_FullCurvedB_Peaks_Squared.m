function phi = errfun_FullCurvedB_Peaks_Squared(freq,weight,est,target,ErrPars)
% Cost function for curve fitting. The cost function consists of three
% contributions:
% e1: Frequency weighted sum of all squared differences along the dB curve
% e2: Sum of squared differences on peak frequency locations
% e3: Sum of squared differences on peak values
%
% -------------------------------- INPUT -------------------------------- %
% freq                      1 x nof array of frequencies 
% weight                    1 x nof array of weights
% est                       n x nof array of the estimated function. n is
%                           number of fitted curves
% target                    n x nof array of the target function
% ErrPars                   Struct containing relevant parameters extracted
%                           from the target curve a priori
%
% -------------------------------- OUTPUT ------------------------------- %
% e                         Error estimate
%
% ----------------------------------------------------------------------- %

% 19.06.2020 NIFD

% Initalize parameters 
phi1 = 0; phi2 = 0; phi3 = 0;
nof = length(freq);

for itarget = 1:size(target,1)
    % Frequency normalized sum of squared differences of the dB values of
    % the fitted and estimated functions for this target curve
    phi1_current = 1/nof*sum((20*log10(weight.*abs(est(itarget,:)))-20*log10(weight.*abs(target(itarget,:)))).^2);
    % Updatae error 1
    phi1 = phi1 + phi1_current;
    

    % Resonance peaks
    [peakloc,peakMag] = Optimization.SupportFunctions.peakfinder(20*log10(abs(est(itarget,:))),1/4,[],1,1,1);
    % Check if resonance peaks exist
    if any(peakloc > 1) && any(peakloc < length(freq))
        % Remove final value
        if peakloc(end) == length(freq)
            peakloc(end) = [];
            peakMag(end) = [];
        end
        if peakloc(1) == 1
            peakloc(1) = [];
            peakMag(1) = [];
        end
        % Peak frequency for estimated function
        maxFreq_e = freq(floor(peakloc)) + ...
            diff(freq(floor(peakloc):ceil(peakloc))).* ...
            (peakloc - floor(peakloc));
        % Peak frequency for target function
        maxFreq_t = ErrPars.PeakFreq{itarget};
        
        % Peak values for estimated and target functions
        maxVal_e = peakMag;
        maxVal_t = ErrPars.PeakVal{itarget};
        
        
       % Sum of squared difference between all peak frequencies
        phi2_current = sum((maxFreq_e - maxFreq_t).^2);
        
        % Sum of squared difference between all peak values
        phi3_current = sum(abs(maxVal_e - maxVal_t).^2);
        
    else % if no peaks are detected in the estimated curve - this might be more elegantly handled by increasing the weight around resonances
        phi2_current = 100*e1_current; % Penalize no-resonance estimated heavily
        phi3_current = 100*e1_current;
    end
    if ~isnan(phi2_current)
        phi2 = phi2 + phi2_current;
    else
        disp('WARNING: Nan values for frequency error est. Consider threshold for peaks')
    end
    if ~isnan(phi3_current)
        phi3 = phi3 + phi3_current;
    else
        disp('WARNING: Nan values for peak values est. Consider threshold for peaks')
    end

end

% Total cost function
phi=1*phi1 + 1*phi2 + 1*phi3;