function P=FitData_MultiTarget_GradientBased(FunHandle, f, target, P_start, m_add, ActiveParams, Weighting)
% New curve-fitting function using a gradient based approach to minize the
% difference between measured and estimated curves
%
% 29.05.2020 NIFD


% Function handle for mathematical model
FitFun = FunHandle;
% Frequency weights
if(nargin <7)
    weighting = ones(size(f));
else
    weighting = makeWeighting(f, Weighting,1);
end

% Find peaks in target curves
for itarget = 1:size(target,1)
    [peakloc,peakMag] = Optimization.SupportFunctions.peakfinder(20*log10(abs(target(itarget,:))),1/4,[],1,1,1);
    % Remove final value
    if peakloc(end) == length(f)
        peakloc(end) = [];
        peakMag(end) = [];
    end
    % Remove initial value
    if peakloc(1) == 1
        peakloc(1) = [];
        peakMag(1) = [];
    end
    
    ErrPars.PeakFreq{itarget} = f(floor(peakloc)) + ...
        diff(f(floor(peakloc):ceil(peakloc))).* ...
        (peakloc - floor(peakloc));
    ErrPars.PeakVal{itarget} = peakMag;
    
end

% Initial guess on parameters
P=P_start;
[NumSets, NumPars] = size(P);
if NumSets > 1
    error('Only one set of initial parameters allowed')
end

% Comment out below - only allow a single parameter set
% Active parameters in optimization
% [RowActive,ColActive] = find(ActiveParams);
% NumActiveParms = zeros(1,NumSets);
%
% for jset = 1:NumSets
%     NumActiveParms(jset) = length(ActiveParams(jset,ColActive(RowActive==jset),:));
% end

% Parameters that are active in optimization
ActiveInd = find(ActiveParams);
NumActiveParms = length(ActiveInd);


% Initial function and error estimate
est = FitFun(f,P,m_add);
Phi=Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, est, target,ErrPars);

% Algorithmic parameters
MaxIterations = 100;
gam = 1e-5;             % Relative parameter increment
pstep = 1;            % Percentage of objective as step size
dPhiTol = 1e-5;

plot_update=1;

% Initialize variables
ErrorEst = zeros(1, MaxIterations);
% ErrorEst(1) = Phi;
ii = 0;
P_it = zeros(MaxIterations,length(P));
P_it(1,:) = P;
dPhi = zeros(NumActiveParms,1);
ddPhi = zeros(NumActiveParms);
beta = 2;
alpha0 = 1;
d = 0;
c1 = 1e-4;
c2 = 0.9;
SemiPosDefTol = 1e-3;
while ii<MaxIterations
    ii=ii+1;
    % Update error estimate function
    ErrorEst(ii) = Phi;
    %     dP = P_it(ii,:);
%         h = gam*min(P_it(ii,ActiveInd));
    %     dP(ActiveInd) = P_it(ii,ActiveInd) + h;
    %                 % Evaluate function at increased parameter
    %             estinc = FitFun(f,dP,m_add);
    %             % Error function evaluated at increased function
    %             Phiinc = Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, estinc, target,ErrPars);
    % Gradients and Hessians by finite differences
%     for jpar = 1:NumActiveParms
%         % Initialize the incremented parameter array
%         %             dP = P(iset,:);
%         dP = P_it(ii,:);
%         dPp = dP;
%         dPm = dP;
%         %             % Step size for considered parameter
%         h = gam*dP(ActiveInd(jpar));
%         % Incremented parameter array
%         dPp(ActiveInd(jpar)) = P_it(ii,ActiveInd(jpar)) + h;
%         dPm(ActiveInd(jpar)) = P_it(ii,ActiveInd(jpar)) - h;
%         % Evaluate function at increased parameter
%         estinc = FitFun(f,dPp,m_add);
%         estdec = FitFun(f,dPm,m_add);
%         % Error function evaluated at increased function
%         Phiinc = Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, estinc, target,ErrPars);
%         Phidec = Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, estdec, target,ErrPars);
%         %             keyboard
%         % Gradient of error function
%         dPhi(jpar) = (Phiinc - Phi)/h;  
%         dPhi2(jpar) = (Phiinc - Phidec)/(2*h);
% %         keyboard
%         for ipar = 1:NumActiveParms
%             dP2 = P_it(ii,:);
%             h2 = gam*dP2(ActiveInd(ipar));
% %             h2 = h;
%             
%             dP2(ActiveInd(ipar)) = dP2(ActiveInd(ipar)) + h2;
%             
%             ddP = P_it(ii,:);
%             ddP(ActiveInd(jpar)) = ddP(ActiveInd(jpar)) + h;
%             ddP(ActiveInd(ipar)) = ddP(ActiveInd(ipar)) + h2;
%             
%             
%             estinc2 = FitFun(f,dP2,m_add);
%             
%             Phiinc2 = Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, estinc2, target,ErrPars);
%             
%             estinc3 = FitFun(f,ddP,m_add);
%             
%             Phiinc3 = Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, estinc3, target,ErrPars);
%             
%             
%             ddPhi(jpar,ipar) = (Phiinc3 - Phiinc2 - Phiinc + Phi)/(h*h2);
% %             keyboard
%         end
% %         keyboard
%     end
    % Test gradient function
    cost = @Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared;
    [Grad,Hess] = Optimization.SupportFunctions.FiniteDifferenceCostGradients(P_it(ii,:),ActiveInd,m_add,f,weighting,FitFun,target,cost,ErrPars);
    
%     keyboard
    dPhi = Grad;
    ddPhi = Hess;
    
    %         keyboard
    %         if ii> 1
    %             beta = (norm(dPhi_new)/norm(dPhi))^2;
    %         end
    %         dPhi = dPhi_new;
    %         if ii> 20
    %             keyboard
    %         end
    % Magnitude of gradient
    MagdPhi = norm(dPhi);
    % Stop loop if gradient is very small
    if MagdPhi < dPhiTol
        break
    end
    % Seach direction
    %         d = -dPhi %+ beta*d;
    lambda = eig(ddPhi);
    lambdatol = length(lambda)*eps(max(lambda));
    
    lambdatolrel = max(lambda)*1e-6;
    
    issemidef = all(lambda >lambdatol);
%     keyboard
    if issemidef      
%         d = -ddPhi\dPhi;
%      mu0 = abs(min(lambda));
%         Happ = ddPhi + (1+0.01)*mu0*eye(NumActiveParms);
%         d = -Happ\dPhi;
%         
        d = -ddPhi\dPhi;
        disp('Using Hessian info')
    else
        iLM = 0;
        Happ = ddPhi;
        lamapp = eig(Happ);
        while any(lamapp<lambdatol)
            iLM = iLM+1;
            mu0 = 0.1*abs(min(lambda));
            Happ = Happ + mu0*eye(NumActiveParms);
            lamapp = eig(Happ);
        end
        
        d = -Happ\dPhi;
        
%         dnewton = -ddPhi\dPhi
%         d = -dPhi;
    end
%     d = -dPhi./lambda;
% d = -abs(ddPhi)\dPhi;
% d = -dPhi;
    d = d';
%             keyboard
    % Step size
    alpha = alpha0;
%             alpha = pstep*Phi/(MagdPhi^2);
    % %         alpha = 0.5;
    % Determine "optimal" step size, alpha
    Pnew = P_it(ii,:);
    Pnew(ActiveInd) = Pnew(ActiveInd) + alpha*d;
    while any(Pnew<0)
        alpha = alpha/beta;
        Pnew = P_it(ii,:);
        Pnew(ActiveInd) = Pnew(ActiveInd) + alpha*d;
        if alpha < eps
            disp('WARNING: Negative parameters never resolved')
            break
        end
    end
    estnew =  FitFun(f,Pnew,m_add);
%     keyboard
    PhiNew = Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, estnew, target,ErrPars);
    
    for jpar = 1:NumActiveParms
        dPnew = Pnew;
        %             % Step size for considered parameter
        hnew = gam*dPnew(ActiveInd(jpar));
%         hnew = h;
        % Incremented parameter array
        dPnew(ActiveInd(jpar)) = Pnew(ActiveInd(jpar)) + hnew;
        % Evaluate function at increased parameter
        estincnew = FitFun(f,dPnew,m_add);
        % Error function evaluated at increased function
        Phiincnew = Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, estincnew, target,ErrPars);
        %             keyboard
        % Gradient of error function
        dPhiNew(jpar,1) = (Phiincnew - PhiNew)/hnew;
    end
    %         keyboard
            ialpha = 0;
            % Strong Wolfe conditions
            while PhiNew > Phi + c1*alpha*d*dPhi%  || abs(d*dPhiNew) > c2*abs(d*dPhi) % PhiNew > Phi% 
                % Shrink the step size
                alpha = 1/beta*alpha;
                Pnew = P_it(ii,:);
            Pnew(ActiveInd) = Pnew(ActiveInd) + alpha*d;
            estnew =  FitFun(f,Pnew,m_add);
%             keyboard
            PhiNew = Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, estnew, target,ErrPars);
%             if PhiNew == Phi
%                    keyboard
%                 [critval,critind] = max(abs(d));
%                 Pnew(ActiveInd(critind)) = Pnew(ActiveInd(critind))*(1+sign(critval)/2);
%                 estnew =  FitFun(f,Pnew,m_add);
%          
%                 PhiNew = Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, estnew, target,ErrPars);
%                 disp('test')
%             
%             end
            for jpar = 1:NumActiveParms
                dPnew = Pnew;
    %             % Step size for considered parameter
                hnew = gam*dPnew(ActiveInd(jpar));
                % Incremented parameter array
                dPnew(ActiveInd(jpar)) = Pnew(ActiveInd(jpar)) + hnew;
                % Evaluate function at increased parameter
                estincnew = FitFun(f,dPnew,m_add);
                % Error function evaluated at increased function
                Phiincnew = Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, estincnew, target,ErrPars);
    %             keyboard
                % Gradient of error function
                dPhiNew(jpar,1) = (Phiincnew - PhiNew)/hnew;
            end
            ialpha = ialpha + 1;
            if ialpha > 200
                break
            end
            end
    
    % Updated parameter array
%     if PhiNew == Phi
%         P_it(ii+1,:) = P_it(ii,:); 
%         [critval,critind] = min(abs(d));
%         
%         P_it(ii+1,ActiveInd(critind)) = P_it(ii+1,ActiveInd(critind))*2^(sign(d(critind)));
%         keyboard
%     else  
    P_it(ii+1,:) = P_it(ii,:);
    P_it(ii+1,ActiveInd) = P_it(ii+1,ActiveInd) + alpha*d;
%     end
    % Updated esimate
    estupdate = FitFun(f,P_it(ii+1,:),m_add);
    % Updated error function
    %         if ii == 16 || ii == 17
    %            keyboard
    %        end
%     if ii > 40
%         keyboard
%     end
    Phi = Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, estupdate, target,ErrPars);
    
    subplot 411
%     plot(1:MaxIterations,ErrorEst)
    hold on
    plot(ii,ErrorEst(ii),'x')
    hold off
    
    subplot 412
%     plot(1:MaxIterations+1,P_it(:,4))
    hold on
    plot(ii+1,P_it(ii+1,4),'x')
    
    plot([1 MaxIterations],[1 1]*12e-3)
    plot([1 MaxIterations],[1 1]*13e-3)
    hold off
    
    subplot 413
%     plot(1:MaxIterations+1,P_it(:,6))
   hold on
    plot(ii+1,P_it(ii+1,6),'x')
 
    plot([1 MaxIterations],[1 1]*2.5e5)
    plot([1 MaxIterations],[1 1]*2e5)
    hold off
    
    subplot 414
    hold on
    plot(ii,lambda(1),'x')
    plot(ii,lambda(2),'o')
    
%     keyboard

pause(0.025)
    
    
end

ErrorEst(ii+1) = Phi;

P = P_it(ii,:)';
% keyboard
figure
subplot 311
plot(1:MaxIterations+1,ErrorEst)
ylabel('\phi')


subplot 312
plot(1:MaxIterations+1,P_it(:,4))
hold on
plot([1 MaxIterations],[1 1]*12e-3)
plot([1 MaxIterations],[1 1]*13e-3)
hold off
ylabel('CW mass, [kg]')

subplot 313
plot(1:MaxIterations+1,P_it(:,6))
hold on
plot([1 MaxIterations],[1 1]*2.5e5)
plot([1 MaxIterations],[1 1]*2e5)
hold off
ylabel('Stiffness, [N/m]')

figure
plot3(P_it(:,4)/13e-3,P_it(:,6)/2e5,ErrorEst)
xlabel('m_j/m_t')
ylabel('k_j/k_t')
zlabel('\phi')
xlim([0.5 3])
ylim([0.5 2])


figure
plot(P_it(:,4)/13e-3,P_it(:,6)/2e5,'b')
hold on
plot(P_it(1,4)/13e-3,P_it(1,6)/2e5,'ro')
plot(P_it(end,4)/13e-3,P_it(end,6)/2e5,'kx')
hold off
xlabel('m_j/m_t')
ylabel('k_j/k_t')
xlim([0.5 3])
ylim([0.5 2])

figure
semilogx(f,abs(target(1,:)))
hold on
semilogx(f,abs(target(2,:)),'--')
semilogx(f,abs(estupdate(1,:)),'-.')
semilogx(f,abs(estupdate(2,:)),'--.')
hold off
%
%         for jj=1:NumPars,
%             for kk=1:NumSets
%                 if(ActiveParams(jj)== 1)
%                     pInx = pInx + 1;
%                     % Update parameter arrays - increase and
%                     % decrease by decay factor N
%                     Pa=P;Pa(jj,kk)=Pa(jj,kk)*N;
%                     Pb=P;Pb(jj,kk)=Pb(jj,kk)/N;
%                     % Function estimates with two parameter sets
%                     esta=FitFun(f,Pa,m_add);
%                     estb=FitFun(f,Pb,m_add);
%                     % Errors for estimated functions
%                     Da=Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, esta, target,ErrPars);
%                     Db=Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting, estb, target,ErrPars);
% %                     keyboard
%                     % Update error limits and parameter array
%                     if Da<Db && Da<D0,
%                         P=Pa;D0=Da;est=esta;
%                     elseif Db<Da && Db<D0,
%                         P=Pb;D0=Db;est=estb;
%                     end
%                     LastPos = (mm-1)*ii_max+(ii-1)*NumActiveParms+pInx;
%                     ErrorEst(LastPos) = D0;
%                 end
%             end
%         end
%         % Visualize curvefitter/optimization procedure every 25 iterations
%         if mod(ii,25)==0
%             if(LastPos <= 25)
%                 break;
%             else
%                 minEst = min(ErrorEst(LastPos-25:LastPos));
%                 maxEst = max(ErrorEst(LastPos-25:LastPos));
%                 if(minEst == maxEst)
%                     done = 1;
%
%                 end
%
%
%                 subplot(4,1,1);
%                 semilogx(f,20*log10(abs(target(1,:))),'b',f,20*log10(abs(est(1,:))),'r',f,20*log10(max(abs(target(1,:)))*weighting/max(weighting)),'g')
%                 hold on
%                 semilogx(ErrPars.PeakFreq{1},ErrPars.PeakVal{1},'o')
%                 hold off
%                 title(sprintf('D_{ave} = %2.3e,  N = %g, M = %g, ii = %g',D0/length(f),N,mm,ii))
%                 grid on
%                 axis tight
%
%                 subplot(4,1,2)
%                 semilogx(f,20*log10(abs(target(2,:))),'b',f,20*log10(abs(est(2,:))),'r',f,20*log10(max(abs(target(2,:)))*weighting/max(weighting)),'g')
%                 hold on
%                 semilogx(ErrPars.PeakFreq{2},ErrPars.PeakVal{2},'o')
%                 hold off
%                 title(sprintf('D_{ave} = %2.3e,  N = %g, M = %g, ii = %g',D0/length(f),N,mm,ii))
%                 grid on
%                 axis tight
%                 if size(target,1)>2
%                   subplot(4,1,3)
%                 semilogx(f,20*log10(abs(target(3,:))),'b',f,20*log10(abs(est(3,:))),'r',f,20*log10(max(abs(target(3,:)))*weighting/max(weighting)),'g')
%                 hold on
%                 semilogx(ErrPars.PeakFreq{3},ErrPars.PeakVal{3},'o')
%                 hold off
%                 title(sprintf('D_{ave} = %2.3e,  N = %g, M = %g, ii = %g',D0/length(f),N,mm,ii))
%                 grid on
%                 axis tight
%
%                 subplot(4,1,4)
%                 semilogx(f,20*log10(abs(target(4,:))),'b',f,20*log10(abs(est(4,:))),'r',f,20*log10(max(abs(target(3,:)))*weighting/max(weighting)),'g')
%                 hold on
%                 semilogx(ErrPars.PeakFreq{4},ErrPars.PeakVal{4},'o')
%                 hold off
%                 title(sprintf('D_{ave} = %2.3e,  N = %g, M = %g, ii = %g',D0/length(f),N,mm,ii))
%                 grid on
%                 axis tight
%                 end
% %                 keyboard
% %                 subplot(3,1,2)
% %                 semilogx(f,20*log10(abs(target(targetno,:)))-20*log10(abs(est(targetno,:))),'r')
% %                 title(sprintf('Difference'))
% %                 grid on
% %                 axis tight
%                 pause(0.01)
%                 if (D0/length(f))<0.001;
%                     ii=ii_max;
%                 end
% %                 subplot(3,1,3)
% %                 plot(ErrorEst);%(1:LastPos));
%
%
%             end
%         end
%     end
% Seems like the donecnt should be replaced by the mm loop?
%         if(done == 1)
%             donecnt = donecnt+1;
%             if(donecnt > M)
%                 break;
%             end
%         end
%     end


% % errest_end = Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(f,weighting,est,target);
% errplot1 = 20*log10(abs(target(1,:)))-20*log10(abs(est(1,:)));
% errplot2 = 20*log10(abs(target(2,:)))-20*log10(abs(est(2,:)));
% normerror = 0;
% for itarget = 1:size(target,1)
%     normerror = normerror + norm(target(itarget,:) -est(itarget,:))/ ...
%         norm(target(itarget,:));
% end
%
% figure
% subplot 211
% semilogx(f,errplot1)
% text(f(round(0.5*length(f))),0.75*max(errplot1),['Error = ' num2str(normerror)])
%
% subplot 212
% semilogx(f,errplot2)
%
% title('Error in dB')
%
% %     weighting
% % Nmax
% % Nmin

end
% function e = Optimization.CostFunctions.errfun_FullCurvedB_Peaks_Squared(freq,weight,est,target,ErrPars)
% 
% % Multi-target function
% 
% e1 = 0; e2 = 0; e3 = 0; e4 = 0; e5 = 0;
% nof = length(freq);
% % keyboard
% for itarget = 1:size(target,1)
%     %     keyboard
%     %     norm1 = sum(20*log10(weight.*abs(target(itarget,:))));
%     %     e1_current = 1/(nof*norm1)*sum(abs(20*log10(weight.*abs(est(itarget,:)))-20*log10(weight.*abs(target(itarget,:)))));
%     %     e1 = e1 + e1_current;
%     %     e1_current = 1/nof*sum(abs((weight.*(abs(est(itarget,:))-abs(target(itarget,:)))./(weight.*abs(target(itarget,:))))));
%     % Frequency point - normalized difference of dB values
% %     e1_current = 1/nof*sum(abs(20*log10(weight.*abs(est(itarget,:)))-20*log10(weight.*abs(target(itarget,:)))));
%     % Smooth objective by using ^2 rather than abs
%     e1_current = 1/nof*sum((20*log10(weight.*abs(est(itarget,:)))-20*log10(weight.*abs(target(itarget,:)))).^2);
% %     e1_current = sum(abs(20*log10(weight.*abs(est(itarget,:)))-20*log10(weight.*abs(target(itarget,:)))));
%     % Squared difference
% %     e1_current = abs(sum((est(itarget,:)) - target(itarget,:)).^2);
%     e1 = e1 + e1_current;
%     %     keyboard
%     %     norm2 = max(20*log10(abs(target(itarget,:))));
%     %     e2_current = 1/norm2*max(abs(20*log10(weight.*abs(est(itarget,:)))-20*log10(weight.*abs(target(itarget,:)))));
%     %
%     %     e2_current = max(abs((weight.*(abs(est(itarget,:))-abs(target(itarget,:)))./(weight.*abs(target(itarget,:))))));
% %         e2_current = max(abs(20*log10(weight.*abs(est(itarget,:)))-20*log10(weight.*abs(target(itarget,:)))));
% %     
% %         e2 = e2 + e2_current;
%     
%     % Weigh in errors on peak frequency
%     %     [maxFreq_t,maxVal_t]=peakExtrap(freq',target(itarget,:));
%     %     [maxFreq_e,maxVal_e]=peakExtrap(freq',est(itarget,:));
%     % Use matlab function findpeaks for faster comp.
%     %     [maxVal_t,maxFreqID_t,peakwidth_t] = findpeaks(20*log10(abs(target(itarget,:))),'Threshold',0.1,'Widthreference','halfprom');
%     %     [maxVal_e,maxFreqID_e,peakwidth_e] = findpeaks(20*log10(abs(est(itarget,:))),'Threshold',0.1,'Widthreference','halfprom');
%     %     npeaks = length(maxVal_t);
%     %     keyboard
%     %     maxFreq_t = freq(maxFreqID_t); maxFreq_e = freq(maxFreqID_e);
%     %     maxFreq_t = ErrPars.PeakFreq{itarget}; maxVal_t = ErrPars.PeakVal{itarget};
%     %     [maxVal_e,maxFreq_e] = findpeaks(20*log10(abs(est(itarget,:))),freq); % no noise on simulated signal
%     %
%     % Resonance peaks
%     [peakloc,peakMag] = Optimization.SupportFunctions.peakfinder(20*log10(abs(est(itarget,:))),1/4,[],1,1,1);
%     % Check if resonance peaks exist
%     if any(peakloc > 1) && any(peakloc < length(freq))
%         
%     
%     % Remove final value
%     if peakloc(end) == length(freq)
%         peakloc(end) = [];
%         peakMag(end) = [];
%     end
%     if peakloc(1) == 1
%         peakloc(1) = [];
%         peakMag(1) = [];
%     end
%     maxFreq_e = freq(floor(peakloc)) + ...
%         diff(freq(floor(peakloc):ceil(peakloc))).* ...
%         (peakloc - floor(peakloc));
%     maxFreq_t = ErrPars.PeakFreq{itarget};
%     
%     maxVal_e = peakMag;
%     maxVal_t = ErrPars.PeakVal{itarget};
%     
%     
%     %     maxValLin_t = abs(target(itarget,maxFreqID_t)); maxValLin_e = abs(est(itarget,maxFreqID_e));
%     % Error on peak frequency
%     %     norm3 = maxFreq_t;
%     %     e3_current = 1/(npeaks)*sum(abs((maxFreq_e - maxFreq_t)./maxFreq_t));
%     % Absolute value of difference
% %     e3_current = sum(abs(maxFreq_e - maxFreq_t));
%     % Smoothed objecive by using ^2
%     e3_current = sum((maxFreq_e - maxFreq_t).^2);
% 
%     % Error on peak value
%     %     norm4 = maxValLin_t;
%     %     e4_current = 1/(npeaks)*sum(abs((maxValLin_e - maxValLin_t)./maxValLin_t));
%     % Absolute value of difference
% %     e4_current = sum(abs(maxVal_e - maxVal_t));
%     % Smooth objective by using squared rather than abs
%     e4_current = sum(abs(maxVal_e - maxVal_t).^2);
%   
%     else % if no peaks are detected in the estimated curve - this might be more elegantly handled by increasing the weight around resonances
%         e3_current = 100*e1_current; % Penalize no-resonance estimated heavily
%         e4_current = 100*e1_current;
%     end
%         if ~isnan(e3_current)
%         e3 = e3 + e3_current;
%     else
%         disp('WARNING: Nan values for frequency error est. Consider threshold for peaks')
%         end
%       if ~isnan(e4_current)
%         e4 = e4 + e4_current;
%     else
%         disp('WARNING: Nan values for peak values est. Consider threshold for peaks')
%     end
%     
%     %      keyboard
%     %      itarget
%     %      e1_current
%     %      e2_current
%     %      e3_current
%     %      e4_current
%     
%     % figure
%     % semilogx(freq,20*log10(abs(est(itarget,:))))
%     % hold on
%     % semilogx(freq,20*log10(abs(target(itarget,:))),'--r')
%     % semilogx(maxFreq_e,maxVal_e,'x')
%     % semilogx(maxFreq_t,maxVal_t,'ro')
%     % hold off
%     %      keyboard
%     % Error on peak width
%     %e5 = e5 + sum(abs(peakwidth_e-peakwidth_t));
% end
% % keyboard
% e=1*e1+1*e2 + 100*e3 + 10*e4 + 1*e5;
% 
% 
% end

% function T=Taper(Level1, Level2, StartInx, EndInx)
% if(Level1 <= Level2)
%     t=-pi/2:pi./(EndInx-StartInx):pi/2;
%     T=(Level2-Level1)*(0.5*(sin(t)+1))+Level1;
% else
%     t=pi/2:-pi./(EndInx-StartInx):-pi/2;
%     T=(Level1-Level2)*(0.5*(sin(t)+1))+Level2;
% end
