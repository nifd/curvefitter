% Script for testing new curvefitter algorithm
% 
% 29.05.2020 NIFD

close all; clearvars; clc;

%% Measurement Data

ImpPath = 'C:\Users\nifd\OneDrive - Demant Group Services\Projects\Ponto\MicroCap\Data\AddedMAssCheck\';
% path(path,'C:\MatLab\Development\ElectroAcousticMechanical\CurveFitter');

%% Input parameters

% Initial guess on parameters
Le = 4e-3;              % Electrical inductance of coil
Re = 5.4;               % Internal resistance of coil
T = 3.9;                % Electro-mechanical transduction coefficient
mskull = 64e-3;         % Mass of skull simulator, abutment and vib-plate
mcw = 11e-3;            % Mass of CounterWeight including EM and nut
cvib = 4e-6;            % Vib spring flexibility, cvib = 1/kvib
kvib = 1/cvib;          % Vib spring stiffness
% kvib = 2.66e5;
bvib = 0.5;               % Vib spring damping

% Array of added masses
m_add = [0 3]*1e-3;

P_MC = [4e-3 5.4 3.9 13e-3 64e-3 1/(5e-6) 0.5 0 0 0 0]';

Uamp = 0.517;
%% Import Data

Data = ImportFunctions.ImpMCData(ImpPath);

nodat = numel(Data);

for idat = 0:nodat-1
    tempDat{idat+1} = Data{nodat-idat};
end
% Data = tempDat;


if nodat ~= length(m_add)
    error('Number of test curves different from added masses')
end


%% Process data


for idat = 1:nodat
    Data{idat}.V_inp = complex(Data{idat}.re_Vinp,Data{idat}.im_Vinp);
    Data{idat}.I_inp = -complex(Data{idat}.re_Iinp,Data{idat}.im_Iinp);
    Data{idat}.Impedance = Data{idat}.V_inp./(Data{idat}.I_inp);
    Data{idat}.v_cw = complex(Data{idat}.re_Icw,Data{idat}.im_Icw);
    Data{idat}.x_cw = 1./(1i*2*pi*Data{idat}.Freq).*Data{idat}.v_cw;
    Data{idat}.a_cw = 1i*2*pi*Data{idat}.Freq.*Data{idat}.v_cw;
    Data{idat}.x_cw_norm = Data{idat}.x_cw./Data{idat}.V_inp;
    
end


%% Curve fitting
P_start = [Le, Re, T, mcw, mskull, kvib, bvib];

ActivePars = [0 0 0 1 0 1 0]; ...
%               0 0 0 1 0 1 0];

f = linspace(100,10000,2000);
% f = Data{1}.Freq;
nof = length(f);

target = zeros(nodat*2,nof);

for idat = 1:nodat
    target(idat,:) = interp1(Data{idat}.Freq,Data{idat}.Impedance,f);
    target(nodat+idat,:) = interp1(Data{idat}.Freq,Data{idat}.x_cw_norm,f);

    
    
end


    FunHandle = @Optimization.Models.FitBAHS_ElImpedance_CWDisp_MultiMasses;
    

    
    P=Optimization.FitData_MultiTarget_GradientBased(FunHandle, ...
        f, target, ...
        P_start, m_add, ActivePars)
    
    P_NoGrad=Optimization.FitData_MultiTarget(FunHandle, ...
        f, target, ...
        P_start, m_add, ActivePars)
    


    Fit.ParVals = P;
    
    Fit.f_n = sqrt(P(6)*(P(4)+m_add+P(5))./((P(4)+m_add)*P(5)))/(2*pi);
    Fit.f_n0 = sqrt(kvib*(mcw+mskull)/(mcw*mskull))/(2*pi);
    Fit.f_nMC = sqrt(P_MC(6)*(P_MC(4)+m_add+P_MC(5))./((P_MC(4)+m_add)*P_MC(5)))/(2*pi);
    
    
    Fit.Pars = {['Le = ' num2str(P(1),'%.2e')]; ...
        ['Re = ' num2str(P(2),'%.2e')]; ...
        ['T = ' num2str(P(3),'%.2e')]; ...
        ['mcw = ' num2str(P(4),'%.2e')]; ...
        ['mskull = ' num2str(P(5),'%.2e')]; ...
        ['kvib = ' num2str(P(6),'%.2e')]
        ['bvib = ' num2str(P(7),'%.2e')];};
    
    Fit.MCPars = {['Le = ' num2str(P_MC(1),'%.2e')]; ...
        ['Re = ' num2str(P_MC(2),'%.2e')]; ...
        ['T = ' num2str(P_MC(3),'%.2e')]; ...
        ['mcw = ' num2str(P_MC(4),'%.2e')]; ...
        ['mskull = ' num2str(P_MC(5),'%.2e')]; ...
        ['kvib = ' num2str(P_MC(6),'%.2e')]
        ['bvib = ' num2str(P_MC(7),'%.2e')];};
    
    Fit.IniPars = {['Le = ' num2str(P_start(1),'%.2e')]; ...
        ['Re = ' num2str(P_start(2),'%.2e')]; ...
        ['T = ' num2str(P_start(3),'%.2e')]; ...
        ['mcw = ' num2str(P_start(4),'%.2e')]; ...
        ['mskull = ' num2str(P_start(5),'%.2e')]; ...
        ['kvib = ' num2str(P_start(6),'%.2e')]
        ['bvib = ' num2str(P_start(7),'%.2e')];};